PYRC
====

PYRC is a collection of classes to write IRC software. The original goal of this project was to write an IRC bot that will provide bacon-themed haiku.

The pyrc.py file contains the infrastructure classes. simple_client.py is a minimalist irc client requiring raw irc commands. bot_base.py will be the base class for bots, but to start is implemented as a single-response bacon-haiku bot. In the future bots will be able to override certain methods to get custom functionality.