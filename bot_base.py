#!/usr/local/bin/python3

import sys
from argparse import ArgumentParser
import pyrc

class BotBase(object):
  def __init__(self, host, port, nickname, channels, options=None):
    self.options = options or pyrc.Options()
    self.advocate = pyrc.Advocate(pyrc.Messenger(host, port, self.options))
    self.nickname = nickname
    self.channels = channels
    self._channels_opened = False
    
  def handle_message(self, msg):
    if msg.command == "376" and not self._channels_opened: #MOTD
      for channel in self.channels:
        self.advocate.join_channel(channel)
      self._channels_opened = True
    else:
      if msg.command == "PRIVMSG":
        recipient = msg.params[0]
        content = msg.params[1]
        if content == "!bacon":
          response = "bacon is tasty / put it in your open mouth / consume it quickly!"
          self.advocate.send_message(recipient, response)
  
  def open(self):
    self.advocate.messenger.open()
    self.advocate.login(self.nickname)
    for msg in self.advocate.read_messages():
      self.handle_message(msg)
  
  def close(self):
    self.advocate.messenger.close()
  
#
def _main():
  arg_parser = ArgumentParser(description="A simple IRC chat bot")
  arg_parser.add_argument("-o", "--host", required=True)
  arg_parser.add_argument("-p", "--port", default=6667, type=int)
  arg_parser.add_argument("-n", "--nickname", required=True)
  arg_parser.add_argument("-d", "--debug", default=False, action="store_true")
  arg_parser.add_argument("channels", nargs="+")
  
  args = arg_parser.parse_args()
  options = pyrc.Options(debug=args.debug)
  bot = BotBase(args.host, args.port, args.nickname, args.channels, options)
  bot.open()
  
if __name__ == "__main__":
  _main()
