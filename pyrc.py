import sys
from telnetlib import Telnet
from threading import Thread

def _debug(subject, content):
  print("{0} >>> {1}".format(subject, content), file=sys.stderr)

class Options(object):
  """Various options for IRC functionality used by pyrc classes."""
  def __init__(self, encoding="utf-8", field_delimiter=" ", 
               message_delimiter="\r\n", automatically_handle_ping=True, 
               read_timeout=5, debug=False):
    self.encoding = encoding
    self.field_delimiter = field_delimiter
    self.message_delimiter = message_delimiter
    self.automatically_handle_ping = automatically_handle_ping
    self.read_timeout = read_timeout
    self.debug = debug
  
#
class Message(object):
  """Object representation for IRC messages."""
  def __init__(self, prefix, command, params=None, options=None):
    self.prefix = prefix
    self.command = command
    self.params = params or []
    self.options = options or Options()
  
  def __str__(self):
    """String representation of a Message object, conforming to IRC standards."""
    start = None
    if self.prefix:
      start = [":" + self.prefix, self.command]
    else:
      start = [self.command]
    params = [i for i in self.params]
    if len(params) and self.options.field_delimiter in params[-1]:
      params[-1] = ":" + params[-1]
    return self.options.field_delimiter.join(start + params)
    
  def encode(self, encoding=None):
    """Returns the byte representation of the message."""
    return (str(self) + self.options.message_delimiter).encode(encoding or self.options.encoding)
  
  @classmethod
  def decode(cls, data, options=None, encoding=None):
    """Decodes a byte stream and returns a Message"""
    if not data:
      return None
    if not options:
      options = Options()
    return cls.parse(data.decode(encoding or options.encoding), options)
  
  @classmethod
  def parse(cls, data, options):
    """Parses a message from data. Data can be either a string or an iterable."""
    if isinstance(data, str):
      return cls.from_string(data, options)
    if hasattr(data, '__len__') and len(data) >= 2:
      return Message(data[0], data[1], data[2:], options)
    return None
  
  @classmethod
  def from_string(cls, data, options):
    """Parses a string into a Message."""
    prefix = None
    command = None
    params = []
    params_index = None
    parts = data.rstrip().split(options.field_delimiter)

    if parts[0][0] == ":":
      prefix = parts[0][1:]
      command = parts[1]
      params_index = 2
    else:
      prefix = None
      command = parts[0]
      params_index = 1
    
    for i in range(params_index, len(parts)):
      if parts[i][0] == ":":
        params.append(options.field_delimiter.join(parts[i:])[1:])
        break
      else:
        params.append(parts[i])
    
    return Message(prefix, command, params, options)
  
#
class Messenger(object):
  """Interacts directly with the IRC server.
  
  Messenger is a context manager that holds a reference to the IRC server and
  contains methods for reading message from, and writing messages to the server.
  """
  def __init__(self, host, port, options=None):
    self.host = host
    self.port = port
    self.options = options or Options()
    self.telnet = None
    self.is_open = False
    self._ru = self.options.message_delimiter.encode(self.options.encoding)
  
  def __enter__(self):
    self.open()
    return self
  
  def __exit__(self, exception_type, exception_value, traceback):
    self.close()
  
  def open(self):
    """Opens a connection to the IRC server."""
    self.telnet = Telnet(self.host, self.port)
    self.is_open = True
  
  def close(self):
    """Closes the connection to the IRC server."""
    if self.is_open:
      self.telnet.close()
      self.is_open = False
      
  def write(self, message):
    """Encodes and writes a message to the server."""
    self.telnet.write(message.encode(self.options.encoding))
    if self.options.debug:
      _debug("SENT", str(message))
  
  def read(self):
    """Reads a message from the server and decodes it as a Message object."""
    message = None
    try:
      message = Message.decode(self.telnet.read_until(self._ru, self.options.read_timeout), self.options)
    except EOFError:
      if self.options.debug:
        _debug("EOF", "Connection closed")
      self.close()
      return None
    
    if self.options.debug:
      _debug("RECEIVED", str(message))
    return message
  
#
class Advocate(object):
  """A wrapper on common IRC messages."""
  def __init__(self, messenger):
    self.messenger = messenger
    self.options = messenger.options
  
  def is_open(self):
    return self.messenger and self.messenger.is_open
  
  def write(self, message):
    """Pass a message to messenger.write."""
    self.messenger.write(message)
  
  def login(self, nickname, full_name=None, password=None):
    """Go through the login process."""
    if password:
      self.write(Message(None, "PASS", [password]))
    self.set_nickname(nickname)
    self.write(Message(None, "USER", [nickname, "8", "*", ":{0}".format(full_name or nickname)]))
  
  def set_nickname(self, nickname):
    self.write(Message(None, "NICK", [nickname]))
      
  def quit(self):
    """Send the QUIT message."""
    self.write(Message(None, "QUIT"))
  
  def send_pong(self, ping_message):
    """Send a PONG message appropriate for given PING message."""
    self.write(Message(None, "PONG", ping_message.params))
  
  def join_channel(self, channel_name):
    """Join a channel."""
    self.write(Message(None, "JOIN", [channel_name]))
  
  def leave_channel(self, channel_name):
    """Leave a channel."""
    self.write(Message(None, "PART", [channel_name]))
  
  def send_message(self, recipient, content):
    """Send a private message to a recipient. Recipient can be a client or channel."""
    self.write(Message(None, "PRIVMSG", [recipient, content]))
  
  def read_messages(self):
    """Generator for messages read from messenger.
    
    If options.automatically_handle_ping is true, appropriate PONG messages
    will be sent to the server when PING messages are received, and the PING
    message will not be yielded.
    """
    while self.messenger.is_open:
      message = self.messenger.read()
      if not message:
        continue
      elif message.command == "PING" and self.options.automatically_handle_ping:
        self.send_pong(message)
      else:
        yield message
  
  @classmethod
  def read_messages_async(cls, advocate, message_handler):
    """Set up a thread to continually read messages, calling message_handler."""
    def thread_target():
      for message in advocate.read_messages():
        message_handler(message, advocate)
    
    read_thread = Thread(target=thread_target)
    read_thread.start()
    return read_thread
  
#
