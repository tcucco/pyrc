#!/usr/local/bin/python3

import sys
import re
from argparse import ArgumentParser
import pyrc

def report_error(msg):
  print(msg, file=sys.stderr)

#
class IRCClient(object):
  def __init__(self, host, port, nickname, options):
    self.options = options or pyrc.Options()
    self.nickname = nickname
    self.messenger = pyrc.Messenger(host, port, options)
    self.advocate = pyrc.Advocate(self.messenger)
    self.current_channel = None
  
  def start(self):
    with self.messenger as messenger:
      self.advocate.login(self.nickname)
      read_thread = pyrc.Advocate.read_messages_async(self.advocate, self.handle_message)
      while self.advocate.is_open():
        self.handle_input(input())
  
  def handle_input(self, raw):
    if not raw:
      pass
    elif raw[0] == "\\":
      self.advocate.write(pyrc.Message.parse(raw[1:], self.options))
    elif raw[0] == "/":
      command, *params = raw.split(" ")
      if command == "/join":
        new_channel = params[0]
        if self.current_channel:
          self.advocate.leave_channel(self.current_channel)
        self.advocate.join_channel(new_channel)
        self.current_channel = new_channel
      elif command == "/part":
        old_channel = params[0]
        if old_channel == self.current_channel:
          self.advocate.leave_channel(old_channel)
          self.current_channel = None
      elif command == "/msg":
        if len(params) >= 2:
          recipient = params[0]
          message = " ".join(params[1:])
          self.advocate.send_message(recipient, message)
        else:
          report_error("To send a message you must provide a recipient and a message")
      elif command == "/quit":
        self.advocate.quit()
      elif command == "/nick":
        self.advocate.set_nickname(params[0])
      else:
        report_error("Unknown command: {0}".format(command))
    else:
      if self.current_channel:
        self.advocate.send_message(self.current_channel, raw)
      else:
        report_error("Error: You are not in a channel so cannot send messages")
  
  def handle_message(self, msg, adv):
    if re.match("^(\\d+|PRIVMSG|NOTICE)$", msg.command):
      self.print_message(msg)
    elif msg.command == "PART" or msg.command == "JOIN":
      user = self.get_user(msg)
      action = "left" if msg.command == "PART" else "joined"
      channel = msg.params[0]
      print("{0} {1} {2}".format(user, action, channel))
    elif msg.command == "QUIT":
      user = self.get_user(msg)
      print("{0} signed out".format(user))
    elif msg.command == "NICK":
      old_nick = self.get_user(msg)
      new_nick = msg.params[0]
      print("{0} is now {1}".format(old_nick, new_nick))
    else:
      print(str(msg))
  
  def print_message(self, msg):
    received_from = None
    is_private = False
  
    if msg.command == "PRIVMSG" or msg.command == "NOTICE":
      received_from = self.get_user(msg)
      is_private = msg.params[0][0] != "#"
    else:
      received_from = "SERVER"
  
    print("{0}{1}: {2}".format("[PRIVATE] " if is_private else "", received_from, " ".join(msg.params[1:])))
  
  def get_user(self, msg):
    return msg.prefix.split("!")[0]
  
#
def _main():
  arg_parser = ArgumentParser(description="A simple IRC client")
  arg_parser.add_argument("-o", "--host", required=True)
  arg_parser.add_argument("-p", "--port", default=6667, type=int)
  arg_parser.add_argument("-n", "--nickname", required=True)
  arg_parser.add_argument("-d", "--debug", default=False, action="store_true")
  
  args = arg_parser.parse_args()
  options = pyrc.Options(debug=args.debug)
  client = IRCClient(args.host, args.port, args.nickname, options)
  client.start()

#
if __name__ == "__main__":
  _main()
